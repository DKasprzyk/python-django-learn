#from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, get_list_or_404
from django.http import HttpResponse
from .models import Pytanie, Odpowiedzi

def index(request):
    last_questions=Pytanie.objects.order_by('data_publikacji')[:5]
    return render(request, 'ankieta/index.html', {'last_questions': last_questions})

def details(request, pytanie_id):
    return HttpResponse("Hej jesteś na stronie szczegółów!! Pytanie: " + str(pytanie_id))
    
def scores(request, pytanie_id):
    return HttpResponse("Hej jesteś na stronie wyniki!! Pytanie: " + str(pytanie_id))
    
def vote(request,  pytanie_id):
    return HttpResponse("Hej jesteś na stronie głosowania!! Pytanie: " + str(pytanie_id))