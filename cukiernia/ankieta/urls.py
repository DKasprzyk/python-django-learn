from django.urls import path
from .import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:pytanie_id>/', views.details, name='details'),
    path('<int:pytanie_id>/scores', views.scores, name='scores'),
    path('<int:pytanie_id>/vote', views.vote, name='vote')
]
