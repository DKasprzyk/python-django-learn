import datetime
from django.db import models
from django.utils import timezone

# Create your models here.


class Pytanie(models.Model):
    text_pytania = models.CharField(max_length=250)
    data_publikacji = models.DateTimeField('data publikacji')
    def __str__(self):
        return self.text_pytania

    def published_recently(self):
        return self.data_publikacji >= timezone.now() - datetime.timedelta(days = 1)

    class Meta:
        verbose_name="Pytanie"
        verbose_name_plural="Pytania"

class Odpowiedzi(models.Model):
    pytanie = models.ForeignKey(Pytanie, on_delete=models.CASCADE)
    tekst_odpowiedzi = models.CharField(max_length=250)
    glosy = models.IntegerField(default = 0)
    def __str__(self):
        return self.tekst_odpowiedzi
    class Meta:
        verbose_name="Odpowiedź"
        verbose_name_plural="Odpowiedzi"
