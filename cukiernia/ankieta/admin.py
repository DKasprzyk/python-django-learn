from django.contrib import admin

# Register your models here.
from .models import Pytanie, Odpowiedzi

admin.site.register(Pytanie)
admin.site.register(Odpowiedzi)